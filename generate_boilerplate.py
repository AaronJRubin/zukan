import yaml
import os
from plant import Plant

def parse_yaml_file(path):
    file = open(path, "r")
    contents = file.read()
    file.close()
    return yaml.load(contents)

plant_data = parse_yaml_file("data/manually_processed/plant_data.yaml")

plant_list = sorted([Plant(kana = kana, **fields) for kana, fields in plant_data.iteritems()], key = lambda plant: plant.kana)

plant_names = [plant.romaji for plant in plant_list]

boilerplate = """{% extends "base/shokubutsu.html" %}
{% block article %}

{% call article_section() %}
{% endcall %}

{% endblock %}"""

for plant in plant_names:
    file = open(os.path.join("templates/sites/plants/shokubutsu", plant + ".html"), "w")
    file.write(boilerplate)
    file.close()
